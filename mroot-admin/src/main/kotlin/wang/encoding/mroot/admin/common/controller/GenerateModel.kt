/*
 * // +-------------------------------------------------------------------------------------------------
 * // |                 有你就好 [ 有节骨乃坚，无心品自端 ]     <http://encoding.wang>
 * // +-------------------------------------------------------------------------------------------------
 * // |                             独在异乡为异客         每逢佳节倍思亲
 * // +-------------------------------------------------------------------------------------------------
 * // |                 联系:   <707069100@qq.com>      <http://weibo.com/513778937>
 * // +-------------------------------------------------------------------------------------------------
 */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                   ErYang出品 属于小极品          共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------


package wang.encoding.mroot.admin.common.controller

import org.apache.commons.lang3.builder.EqualsBuilder
import org.apache.commons.lang3.builder.HashCodeBuilder
import org.apache.commons.lang3.builder.ToStringBuilder

import java.io.Serializable


/**
 * 用于自动生成代码的 model
 * @author ErYang
 */
class GenerateModel : Serializable {

    companion object {

        private const val serialVersionUID = 4801248565510630859L
    }

    // -------------------------------------------------------------------------------------------------


    /**
     * 名称
     */
    var name: String? = null
    /**
     * 名称类型
     */
    var type: String? = null
    /**
     * 骆驼命名法
     */
    var camelCaseName: String? = null
    /**
     * 首字母大写命名法
     */
    var firstCapitalizeCamelCaseName: String? = null
    /**
     * 名称的常量
     */
    var finalName: String? = null
    /**
     * 数据库ID
     */
    var dataId: String? = null
    /**
     * 数据类型
     */
    var dataType: String? = null
    /**
     * 名称备注
     */
    var comment: String? = null
    /**
     * 默认值
     */
    var defaultValue: String? = null

    // -------------------------------------------------------------------------------------------------

    override fun equals(other: Any?): Boolean {
        if (this === other) return true

        if (other !is GenerateModel) return false

        val that: GenerateModel? = other

        return EqualsBuilder()
                .append(name, that?.name)
                .append(type, that?.type)
                .append(dataType, that?.dataType)
                .append(comment, that?.comment)
                .append(defaultValue, that?.defaultValue)
                .append(firstCapitalizeCamelCaseName, that?.firstCapitalizeCamelCaseName)
                .append(camelCaseName, that?.camelCaseName)
                .append(finalName, that?.finalName)
                .append(dataId, that?.dataId)
                .isEquals
    }

    // -------------------------------------------------------------------------------------------------

    override fun hashCode(): Int {
        return HashCodeBuilder(17, 37)
                .append(name)
                .append(dataType)
                .append(type)
                .append(comment)
                .append(defaultValue)
                .append(firstCapitalizeCamelCaseName)
                .append(camelCaseName)
                .append(finalName)
                .append(dataId)
                .toHashCode()
    }

    // -------------------------------------------------------------------------------------------------

    override fun toString(): String {
        return ToStringBuilder(this)
                .append("name", name)
                .append("type", type)
                .append("dataType", dataType)
                .append("comment", comment)
                .append("defaultValue", defaultValue)
                .append("firstCapitalizeCamelCaseName", firstCapitalizeCamelCaseName)
                .append("camelCaseName", camelCaseName)
                .append("finalName", finalName)
                .append("dataId", dataId)
                .toString()
    }

    // -------------------------------------------------------------------------------------------------

}

// -----------------------------------------------------------------------------------------------------

// End GenerateModel class

/* End of file GenerateModel.kt */
/* Location: ./src/main/kotlin/wang/encoding/mroot/admin/common/controller/GenerateModel.kt */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                           ErYang出品 属于小极品  O(∩_∩)O~~   共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------
