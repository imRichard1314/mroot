<#-- 页面提示信息模板函数 -->
<div class="m-form__content">
    <div class="m-alert m-alert--icon alert alert-danger m--hide" role="alert" id="form_msg">
        <div class="m-alert__icon">
            <i class="la la-warning"></i>
        </div>
        <div class="m-alert__text" id="alert_text">
        ${I18N("message.form.alert.content")}
        </div>
        <div class="m-alert__close">
            <button type="button" class="close" data-close="alert"
                    aria-label="Close"></button>
        </div>
    </div>
</div>
