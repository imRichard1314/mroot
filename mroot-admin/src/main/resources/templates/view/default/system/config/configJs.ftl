<script>

    /**
     * 系统配置提示信息
     */
    var ConfigValidation = function () {


        var type_range = '${I18N("jquery.validation.type.range")}';

        var name_pattern = '${I18N("jquery.validation.name.pattern")}';

        var title_pattern = '${I18N("jquery.validation.title.pattern")}';

        var value_pattern = '${I18N("jquery.validation.system.config.value.pattern")}';

        var status_range = '${I18N("jquery.validation.status.range")}';

        var sort_range = '${I18N("jquery.validation.sort.range")}';

        var remark_pattern = '${I18N("jquery.validation.remark.pattern")}';

        return {


            getTypeRange: function () {
                return type_range;
            },

            getNamePattern: function () {
                return name_pattern;
            },

            getTitlePattern: function () {
                return title_pattern;
            },

            getValuePattern: function () {
                return value_pattern;
            },

            getStatusRange: function () {
                return status_range;
            },

            getSortRange: function () {
                return sort_range;
            },

            getRemarkPattern: function () {
                return remark_pattern;
            }


            // -------------------------------------------------------------------------------------------------

        }
    }();

    // -------------------------------------------------------------------------------------------------

</script>
