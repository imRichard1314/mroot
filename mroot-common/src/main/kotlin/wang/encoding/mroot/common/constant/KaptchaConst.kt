/*
* // +-------------------------------------------------------------------------------------------------
* // |                 有你就好 [ 有节骨乃坚，无心品自端 ]     <http://encoding.wang>
* // +-------------------------------------------------------------------------------------------------
* // |                             独在异乡为异客         每逢佳节倍思亲
* // +-------------------------------------------------------------------------------------------------
* // |                 联系:   <707069100@qq.com>      <http://weibo.com/513778937>
* // +-------------------------------------------------------------------------------------------------
*/

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                   ErYang出品 属于小极品          共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------


package wang.encoding.mroot.common.constant


import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.cache.annotation.EnableCaching
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.PropertySource

import javax.validation.constraints.NotBlank


/**
 * kaptcha 验证码配置文件
 *
 * @author ErYang
 */
@Configuration
@PropertySource(value = ["classpath:kaptcha.properties"])
@ConfigurationProperties(prefix = "kaptcha")
@EnableCaching
class KaptchaConst {

    /**
     * 边框
     */
    @NotBlank
    lateinit var border: String

    /**
     * 字体颜色
     */
    @NotBlank
    lateinit var textProducerFontColor: String

    /**
     * 字体尺寸
     */
    @NotBlank
    lateinit var textProducerFontSize: String

    /**
     * 字体名称
     */
    @NotBlank
    lateinit var textProducerFontNames: String

    /**
     * 验证码长度
     */
    @NotBlank
    lateinit var textProducerCharLength: String

    /**
     * 验证之间的空格
     */
    @NotBlank
    lateinit var textProducerCharSpace: String

    /**
     * 验证码内容
     */
    @NotBlank
    lateinit var textProducerCharString: String

    /**
     * 图片宽度
     */
    @NotBlank
    lateinit var imageWidth: String

    /**
     * 图片高度
     */
    @NotBlank
    lateinit var imageHeight: String

    /**
     * 背景颜色渐变 开始颜色
     */
    @NotBlank
    lateinit var backgroundClearFrom: String

    /**
     * 背景颜色渐变 结束颜色
     */
    @NotBlank
    lateinit var backgroundClearTo: String

    /**
     * 噪点颜色
     */
    @NotBlank
    lateinit var noiseColor: String

    /**
     * 噪点实现类
     */
    @NotBlank
    lateinit var noiseImpl: String
    /**
     * 图片样式
     */
    @NotBlank
    lateinit var obscurificatorImpl: String

    /**
     * session key
     */
    @NotBlank
    lateinit var sessionKey: String

    /**
     * session date
     */
    @NotBlank
    lateinit var sessionDate: String

    // -------------------------------------------------------------------------------------------------

}

// -----------------------------------------------------------------------------------------------------

// End KaptchaConst class

/* End of file KaptchaConst.kt */
/* Location: ./src/main/kotlin/wang/encoding/mroot/common/constant/KaptchaConst.kt */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                           ErYang出品 属于小极品  O(∩_∩)O~~   共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------
