/*
* // +-------------------------------------------------------------------------------------------------
* // |                 有你就好 [ 有节骨乃坚，无心品自端 ]     <http://encoding.wang>
* // +-------------------------------------------------------------------------------------------------
* // |                             独在异乡为异客         每逢佳节倍思亲
* // +-------------------------------------------------------------------------------------------------
* // |                 联系:   <707069100@qq.com>      <http://weibo.com/513778937>
* // +-------------------------------------------------------------------------------------------------
*/

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                   ErYang出品 属于小极品          共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------


package wang.encoding.mroot.common.config

import com.baomidou.mybatisplus.plugins.PaginationInterceptor
import com.baomidou.mybatisplus.plugins.PerformanceInterceptor
import org.mybatis.spring.annotation.MapperScan
import org.springframework.cache.annotation.EnableCaching
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Profile


/**
 * mybatis plus 配置
 *
 * @author ErYang
 */
@Configuration
@EnableCaching
@MapperScan("wang.encoding.mroot.mapper*")
class MybatisPlusConfiguration {

    /**
     * SQL执行效率插件【生产环境可以关闭】
     */
    @Bean
    // 设置 dev test 环境开启
    @Profile("dev")
    fun performanceInterceptor(): PerformanceInterceptor {
        return PerformanceInterceptor()
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 分页插件
     */
    @Bean
    fun paginationInterceptor(): PaginationInterceptor {
        val paginationInterceptor = PaginationInterceptor()
        // 开启 PageHelper 的支持
        paginationInterceptor.setLocalPage(true)
        return paginationInterceptor
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 注入公共字段自动填充
     */
//    @Bean
//    fun metaObjectHandler(): MetaObjectHandler {
//        return MybatisPlusMetaObjectHandler()
//    }

    // -------------------------------------------------------------------------------------------------

}

// -----------------------------------------------------------------------------------------------------

// End MybatisPlusConfiguration class

/* End of file MybatisPlusConfiguration.kt */
/* Location: ./src/main/kotlin/wang/encoding/mroot/admin/common/config/MybatisPlusConfiguration.kt */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                           ErYang出品 属于小极品  O(∩_∩)O~~   共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------
