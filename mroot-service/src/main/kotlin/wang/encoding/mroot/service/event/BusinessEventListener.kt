/*
 * // +-------------------------------------------------------------------------------------------------
 * // |                 有你就好 [ 有节骨乃坚，无心品自端 ]     <http://encoding.wang>
 * // +-------------------------------------------------------------------------------------------------
 * // |                             独在异乡为异客         每逢佳节倍思亲
 * // +-------------------------------------------------------------------------------------------------
 * // |                 联系:   <707069100@qq.com>      <http://weibo.com/513778937>
 * // +-------------------------------------------------------------------------------------------------
 */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                   ErYang出品 属于小极品          共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------


package wang.encoding.mroot.service.event


import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.event.EventListener
import org.springframework.scheduling.annotation.Async
import org.springframework.stereotype.Component
import wang.encoding.mroot.service.system.*

/**
 * 事件监听器
 *
 * @author ErYang
 */
@Component
class BusinessEventListener {


    @Autowired
    private lateinit var requestLogService: RequestLogService


    @Autowired
    private lateinit var configService: ConfigService

    @Autowired
    private lateinit var ruleService: RuleService


    /**
     * 请求日志事件
     *
     * @param requestLogAopEvent RequestLogAopEvent
     */
    @Async
    @EventListener
    //@Throws(ServiceException::class)
    fun saveRequestLog(requestLogAopEvent: RequestLogAopEvent) {
        if (null != requestLogAopEvent.requestLog) {
            requestLogService.save(requestLogAopEvent.requestLog!!)
        }
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 清除 Config 缓存事件
     *
     * @param removeConfigCacheEvent RemoveConfigCacheEvent
     */
    @Async
    @EventListener
    fun removeConfigCacheById(removeConfigCacheEvent: RemoveConfigCacheEvent) {
        if (null != removeConfigCacheEvent.id) {
            configService.removeCacheById(removeConfigCacheEvent.id!!)
        }
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 批量清除 Config 缓存事件
     *
     * @param removeConfigCacheEvent RemoveConfigCacheEvent
     */
    @Async
    @EventListener
    fun removeBatchConfigCacheById(removeConfigCacheEvent: RemoveConfigCacheEvent) {
        if (null != removeConfigCacheEvent.idArray) {
            configService.removeBatchCacheById(removeConfigCacheEvent.idArray!!)
        }
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 清除 Rule 缓存事件
     *
     * @param removeRuleCacheEvent RemoveRuleCacheEvent
     */
    @Async
    @EventListener
    fun removeRuleCacheById(removeRuleCacheEvent: RemoveRuleCacheEvent) {
        if (null != removeRuleCacheEvent.id) {
            ruleService.removeCacheById(removeRuleCacheEvent.id!!)
        }
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 批量清除 Rule 缓存事件
     *
     * @param removeRuleCacheEvent RemoveRuleCacheEvent
     */
    @Async
    @EventListener
    fun removeBatchRuleCacheById(removeRuleCacheEvent: RemoveRuleCacheEvent) {
        if (null != removeRuleCacheEvent.idArray) {
            ruleService.removeBatchCacheById(removeRuleCacheEvent.idArray!!)
        }
    }

    // -------------------------------------------------------------------------------------------------

}

// -----------------------------------------------------------------------------------------------------

// End EventListener class

/* End of file EventListener.kt */
/* Location: ./src/main/kotlin/wang/encoding/mroot/service/event/EventListener.kt */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                           ErYang出品 属于小极品  O(∩_∩)O~~   共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------

// -----------------------------------------------------------------------------------------------------
