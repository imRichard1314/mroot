/*
* // +-------------------------------------------------------------------------------------------------
* // |                 有你就好 [ 有节骨乃坚，无心品自端 ]     <http://encoding.wang>
* // +-------------------------------------------------------------------------------------------------
* // |                             独在异乡为异客         每逢佳节倍思亲
* // +-------------------------------------------------------------------------------------------------
* // |                 联系:   <707069100@qq.com>      <http://weibo.com/513778937>
* // +-------------------------------------------------------------------------------------------------
*/

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                   ErYang出品 属于小极品          共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------


package wang.encoding.mroot.service.cms


import wang.encoding.mroot.model.entity.cms.ArticleContent
import wang.encoding.mroot.common.service.BaseService

import java.math.BigInteger

/**
 * 后台 文章内容 Service 接口
 *
 * @author ErYang
 */
interface ArticleContentService : BaseService<ArticleContent> {


    /**
     * 初始化新增 ArticleContent 对象
     *
     * @param articleContent ArticleContent
     * @return ArticleContent
     */
    fun initSaveArticleContent(articleContent: ArticleContent): ArticleContent

    // -------------------------------------------------------------------------------------------------

    /**
     * 初始化修改 ArticleContent 对象
     *
     * @param articleContent ArticleContent
     * @return ArticleContent
     */
    fun initEditArticleContent(articleContent: ArticleContent): ArticleContent

    // -------------------------------------------------------------------------------------------------

    /**
     * Hibernate Validation 验证
     */
    fun validationArticleContent(articleContent: ArticleContent): String?

    // -------------------------------------------------------------------------------------------------

    /**
     * 新增 文章内容
     *
     * @param articleContent ArticleContent
     * @return ID  BigInteger
     */
    fun saveBackId(articleContent: ArticleContent): BigInteger?

    // -------------------------------------------------------------------------------------------------

    /**
     * 更新 文章内容
     *
     * @param articleContent ArticleContent
     * @return ID  BigInteger
     */
    fun updateBackId(articleContent: ArticleContent): BigInteger?

    // -------------------------------------------------------------------------------------------------

    /**
     * 删除 文章内容 (更新状态)
     *
     * @param id BigInteger
     * @return ID  BigInteger
     */
    fun removeBackId(id: BigInteger): BigInteger?

    // -------------------------------------------------------------------------------------------------

    /**
     * 恢复 文章内容 (更新状态)
     *
     * @param id BigInteger
     * @return ID  BigInteger
     */
    fun recoverBackId(id: BigInteger): BigInteger?

    // -------------------------------------------------------------------------------------------------

}

// -----------------------------------------------------------------------------------------------------

// End ArticleContentService interface

/* End of file ArticleContentService.kt */
/* Location: ./src/main/kotlin/wang/encoding/mroot/service/cms/ArticleContent.kt */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                           ErYang出品 属于小极品  O(∩_∩)O~~   共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------
